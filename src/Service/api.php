<?php
include("conectaDB.php");

#Exibir dados de Estadao/Cidade
$sql=mysqli_query($conect,"SELECT e.id as idEstado, e.nome as nomeEstado, e.uf, e.flag_map, c.data_criacao, c.data_alteracao, c.id as idCidade, c.nome as nomeCidade
FROM 
    estado as e,
    cidade as c
WHERE
    c.id_estado = e.id
ORDER BY
    c.nome");
$J=[];
$I=0;
while($ln=mysqli_fetch_array($sql)){
    $J[$I] = [
        "id"=>$ln["idCidade"],
        "nome-estado"=>$ln["nomeEstado"],
        "uf"=>$ln["uf"],
        "nome-cidade"=>$ln["nomeCidade"],
        "data_criacao"=>$ln["data_criacao"],
        "data_alteracao"=>$ln["data_alteracao"],
        "flag_map"=>$ln["flag_map"],
    ];
    $I++;
}

header("Access-Control-Allow-Origin:*");
header("Content-type: application/json");

echo json_encode($J);