import React from 'react';
import './index.css';

function Footer() {
  return (
    <footer className="footer">
      <div className="wrap">
          <p className="footer__copyright">© Zoox 2019 <span>-</span> <span>Todos os direitos reservados</span></p>
      </div>
    </footer>
  );
}

export default Footer;