import React from 'react';
import axios from "axios";
import './index.css';

import Tabs from './tabs';
import Message from './message';

class Forms extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isActive : '1',
            nameState: '',
            ufState: '',
            msg: '',
            typeMessage: '',
            states: [],
            idState: 'Escolha o estado',
            idCity: '',
            modal: false,
            editCity: false,
            city: {}
        }
    }

    getMsg = (msg, typeMessage) => {
        let $this = this;
        this.setState({msg, typeMessage});
        setTimeout (function(){
            $this.setState({msg: '', typeMessage: ''});
        }, 5000)
    }

    getState = async () => {
        const response = await axios.get('http://washingtonluiz.com.br/zoox/lists/listState.php');
        this.setState({states: response.data});
    }

    getCity = async (item) => {
        const response = await axios.get(`http://washingtonluiz.com.br/zoox/lists/listCity.php?idCity=${item}`);
        this.setState({city: response.data});
    }

    handleActive = (event) => {
        this.setState({isActive: event.target.id});
    }

    handleRegisterState = async (event) => {
        event.preventDefault();
        let $this = this;
        let formData = new FormData();
        formData.append('nameState', this.state.nameState)
        formData.append('ufState', this.state.ufState)

        axios({
            method: 'post',
            url: 'http://washingtonluiz.com.br/zoox/register/registerState.php',
            data: formData,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        })
        .then(function (response) {
            if(response.data.status){
                $this.getMsg(response.data.mensagem, "success");
                $this.setState({nameState: '', ufState: ''})
                $this.getState();
            }else{
                $this.getMsg(response.data.mensagem, "error")
            }
           
        })
        .catch(function (response) {
            console.log(response)
        });
    }

    handleRegisterCity = (event) =>{
        event.preventDefault();
        let $this = this;
        let formData = new FormData();
        formData.append('nameCity', this.state.nameCity)
        formData.append('idState', this.state.idState)

        axios({
            method: 'post',
            url: 'http://washingtonluiz.com.br/zoox/register/registerCity.php',
            data: formData,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        })
        .then(function (response) {
            if(response.data.status){
                $this.getMsg(response.data.mensagem, "success");
                $this.setState({idState: 'Escolha o estado', nameCity: ''});
                $this.props.getStates();
            }else{
                $this.getMsg(response.data.mensagem, "error")
            }
           
        })
        .catch(function (response) {
            console.log(response)
        });
    }

    handleDeleteCity = () => {
        let $this = this;
            axios({
                method: 'get',
                url: `http://washingtonluiz.com.br/zoox/delete/deleteCity.php?idCity=${this.state.idCity}`
            })
            .then(function (response) {
                if(response.data.status){
                    $this.getMsg(response.data.mensagem, "success");
                    $this.props.getStates();
                    $this.setState({modal: !$this.state.modal});
                }else{
                    $this.getMsg(response.data.mensagem, "error")
                }
            
            })
            .catch(function (response) {
                console.log(response)
            });
    }

    handleEditCity = (idCity) => {
        this.getCity(idCity);
        this.setState({editCity: !this.state.editCity});
    }

    componentDidMount() {
        this.getState();
    }
    render(){
        const { isActive, msg, typeMessage, states, idState, modal, editCity } = this.state;
        const { listStates } = this.props;
        return(
            <div className="forms">
                {modal &&
                    <section id="modal" className="">
                        <div className="box-modal box-msg">
                            <div className="modalContent">
                                <header className="header-modal">
                                    <h1 className="titulo-modal">Exclusão!</h1>
                                    <span className="close jclose fr">×</span>
                                </header>
                                <div className="content-modal">
                                    Tem certeza que deseja excluir ?
                                </div>
                                <footer className="footer-modal">
                                    <button className="btn success" onClick={e => this.handleDeleteCity()}>Sim</button>
                                    <button className="btn error" onClick={e => this.setState({modal: !modal})}>Não</button>
                                </footer>
                            </div>
                        </div>
                    </section>
                }
                    
                {msg !== '' &&
                    <Message message={msg} typeMessage={typeMessage} />
                }

                {editCity &&
                    <div className="">
                    <span onClick={e => this.setState({editCity: !editCity})}><i className="fa fa-arrow-left" aria-hidden="true"></i> Voltar</span>
                    <p>Infelizmente não consegui terminar como gostaria. :(</p>
                    </div>
                }
                {!editCity &&
                    <div>
                        <Tabs handleActive={this.handleActive} isActive={isActive} />
                        <div className={isActive === '1' ? 'tab tabActive' : 'tab'}>
                            <h2>Cadastro Estado</h2>

                            <form type="post" id="frmRegisterState">
                                <div className="box-input">
                                    <input type="text" id="nameState" name="nameState" placeholder="Digite o nome do estado" onChange={e => this.setState({ nameState: e.target.value })} required />
                                </div>
                                <div className="box-input">
                                    <input type="text" id="ufState" name="ufState" placeholder="Digite a UF" maxLength="2" onChange={e => this.setState({ ufState: e.target.value })}    required />
                                </div>
                                <input type="submit" value="Cadastrar" onClick={e => this.handleRegisterState(e)} />
                            </form>
                        </div>
                        <div className={isActive === '2' ? 'tab tabActive' : 'tab'}>
                            <h2>Cadastro Cidade</h2>
                            <form type="post" id="frmRegisterState">
                                <div className="box-input">
                                    <input type="text" id="nameCity" name="nameCity" placeholder="Digite o nome da cidade" onChange={e => this.setState({ nameCity: e.target.value })} required />
                                </div>
                                <div className="box-input">
                                    <div className="styled-select">
                                        <select value={idState} onChange={e => this.setState({ idState: e.target.value })}>
                                            <option value="Escolha o estado">{idState}</option>
                                            {states.map((item) => {
                                                return <option key={item.id} value={item.id}>{`${item['nome-estado']} - ${item.uf}`}</option>
                                            })}
                                        </select>
                                    </div>
                                </div>
                                <input type="submit" value="Cadastrar" onClick={e => this.handleRegisterCity(e)} />
                            </form>
                        </div>

                        <div className={isActive === '3' ? 'tab tabActive' : 'tab'}>
                        <h2>Lista das cidades</h2>

                        <ul className="list-cities">
                        {listStates &&
                            listStates.map((item, index) => {
                                return (
                                <li key={item.id}>
                                    <div className="list-cities__data">
                                        <div>Nome cidade: <span>{item['nome-cidade']}</span></div>
                                        <div>Nome estado: <span>{item['nome-estado']}</span></div>
                                        <div>Uf: <span>{item.uf}</span></div>
                                        <div>Data da criação: <span>{item.data_criacao}</span></div>
                                        <div>Data da alteração: <span>{item.data_alteracao}</span></div>
                                    </div>
                                    <div className="list-cities__action">
                                        <span className="btn list-cities__action--alter" onClick={e => this.handleEditCity(item.id)}><i className="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</span>
                                        <span className="btn list-cities__action--delete" onClick={e => this.setState({idCity: item.id, modal: !this.state.modal})}><i className="fa fa-times" aria-hidden="true"></i> Excluir</span>
                                    </div>
                                </li>
                                )
                            })
                        }
                        {listStates.length === 0 &&
                            <li>Não há cidades cadastradas</li>
                        }
                        </ul>
                    </div>
                    </div>
                }
            </div>
        );
    }
  
}

export default Forms;