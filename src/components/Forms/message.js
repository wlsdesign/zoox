import React from 'react';
import './message.css';

class Message extends React.Component {
    render(){
        return(
            <div className={`message ${this.props.typeMessage}`}>
            <p>{this.props.message}</p>
            </div>
        );
    }
  
}

export default Message;