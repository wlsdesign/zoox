import React from 'react';
import './tabs.css';

class Tabs extends React.Component {
    render(){
        return(
            <div className="box-tabs">
                <ul className="tabs">
                    <li data-mobile="tab--mobile" className={this.props.isActive === '3' ? 'tabs--item tab--active' : 'tabs--item'} id='3' onClick={this.props.handleActive}>Visualizar Cidades</li>
                    <li className={this.props.isActive === '1' ? 'tabs--item tab--active' : 'tabs--item'} id='1' onClick={this.props.handleActive}>Cadastrar Estado</li>
                    <li className={this.props.isActive === '2' ? 'tabs--item tab--active' : 'tabs--item'} id="2" onClick={this.props.handleActive}>Cadastrar Cidade</li>
                </ul>
            </div>
        );
    }
  
}

export default Tabs;