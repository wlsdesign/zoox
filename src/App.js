import React from 'react';
import './css/variables.css';
import './css/buttons.css';
import './css/reset.css';
import './App.css';

import Header from './components/Header';
import Footer from './components/Footer';
import Map from './components/Map';
import Forms from './components/Forms';
  

class App extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {
      listStates: []
    }
  }

  getStates = () => {
    fetch("http://washingtonluiz.com.br/zoox/api.php")
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({listStates: responseJson});
    });
  }

  componentDidMount(){
    this.getStates();
  }

  render(){
    const { listStates } = this.state;
    return (
      <div className="container">
        <Header handleSearch={this.handleSearch} />
        <div className="wrap cotainer__content">
          <Map />
          <div className="box-crud">
            <Forms listStates={listStates} getStates={this.getStates} />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
